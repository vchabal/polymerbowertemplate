Polymer Bower Template

You can integrate this element into your project by adding following dependency to bower.js file:
"polymer-bower-template": "https://git@bitbucket.org/vchabal/polymerbowertemplate.git"

Required Dependencies:
- Polymer
- Raphael.js

wiki: https://bitbucket.org/vchabal/polymerbowertemplate/wiki/Home
